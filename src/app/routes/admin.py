from __future__ import annotations

from flask import Blueprint, session, redirect, url_for, render_template, flash, g

from src.app.controllers import OrderController
from src.app.routes.user import login_required

admin_area_blueprint = Blueprint("admin_area", __name__, url_prefix="/admin")
admin_product_info_blueprint = Blueprint("admin_product_info", __name__, url_prefix="/admin/products")
admin_orders_info_blueprint = Blueprint("admin_orders_info", __name__, url_prefix="/admin/orders")


@admin_area_blueprint.route("/")
@login_required
def admin_area():
    first_name = session["first_name"]
    if "admin" not in first_name:
        flash("You are not an admin", "warning")
        return redirect(url_for('root.root'))
    return render_template('adminArea.html', firstName=first_name, noOfItems=session["no_of_items"],
                           loggedIn=session["logged_in"])


@admin_product_info_blueprint.route("/")
@login_required
def admin_available_products_statistics():
    return redirect(url_for('root.root'))


@admin_orders_info_blueprint.route("/")
def admin_orders_statistics():
    ses = g.session
    orders = OrderController(ses).get_orders_with_product_info()

    dynamic_data_table = get_dynamic_table(orders)

    return render_template("adminOrderStats.html", cloumnHeaders=dynamic_data_table[0],
                           tableValues=dynamic_data_table[1:],
                           firstName=session["first_name"], noOfItems=session["no_of_items"],
                           loggedIn=session["logged_in"])


def get_dynamic_table(orders: list[list[str | int]]) -> list[list[str | int]]:
    """
    orders besteht aus einer Liste aus Listen. Eine Liste besteht jeweils aus einer order_item_id (int), einer order_id (int) und
    dem product_name (str).

    :param orders: list[list[str | int]] Rohe Daten, aus welchen eine Statistik erstellt wird
    :return: das 2d Array mit der Statistik mit den product_name als Spaltennamen und der order_id als Reihennamen
    """
    if not orders:
        return []

    product_names: set[str] = set()
    orders_dict: dict[int, dict[str, int]] = {}

    for order_item_id, order_id, product_name in orders:
        # Extract the product type from the product name
        product_type = product_name.split(': ')[0]
        product_names.add(product_type)

        # Initialize order entry if not present
        if order_id not in orders_dict:
            orders_dict[order_id] = {}
        # Increment the count of the product type for the given order ID
        if product_type not in orders_dict[order_id]:
            orders_dict[order_id][product_type] = 0
        orders_dict[order_id][product_type] += 1

    # Sort product names lexicographically
    product_names = sorted(product_names)
    column_headers: list[str | int] = ["Order ID"] + [f"{product}: Test Product" for product in product_names] + [
        "Total"]

    dynamic_table: list[list[str | int]] = [column_headers]

    # Fill 2D array with values, where column headers are product name and row headers are order id
    for order_id in sorted(orders_dict.keys()):
        row: list[str | int] = [order_id]
        total_products = 0
        for product in product_names:
            count = orders_dict[order_id].get(product, 0)
            row.append(count)
            total_products += count
        row.append(total_products)
        dynamic_table.append(row)

    # Create total row containing the total for each column
    total_row: list[str | int] = ["Total"]
    total_per_product = [0] * (len(product_names) + 1)  # Initialize total counts for each product + total column
    for row in dynamic_table[1:]:
        for i in range(1, len(row)):
            total_per_product[i - 1] += row[i]
    total_row.extend(total_per_product)

    dynamic_table.append(total_row)

    return dynamic_table if len(dynamic_table) > 1 else []


def calculate_row_totals(dynamic_table: list[list[str | int]]) -> list[list[str | int]]:
    return dynamic_table


def calculate_column_totals(dynamic_table: list[list[str | int]]) -> list[list[str | int]]:
    pass
